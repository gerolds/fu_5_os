# Betriebssysteme, WS 2017/18, Barry Linnert
# Author: Gerold Schneider
#
# All recipes assume:
# 	1. the appropriate QEMU toolchain is available below /home/mi/linnert/arm
#   2. the directory containing this Makefile is writable for the user
#
# Recipes:
#     "except" builds the exception handling problem
#

TOOL_PATH = /home/mi/linnert/arm/bin
BUILD_PATH = ./bin
SOURCE_PATH = ./source
LD_ALIAS = arm-none-eabi-ld
LD_FLAGS = -t# -nostdlib -nodefaultlibs -L/home/mi/linnert/arm/lib/gcc/arm-none-eabi/4.8.3 -lgcc
LD = $(TOOL_PATH)/$(LD_ALIAS) $(LD_FLAGS)
LIBGCC = /home/mi/linnert/arm/lib/gcc/arm-none-eabi/4.8.3/libgcc.a
OBJDUMP_ALIAS = arm-none-eabi-objdump
OBJDUMP_FLAGS = -Dfh
OBJDUMP = $(TOOL_PATH)/$(OBJDUMP_ALIAS) $(OBJDUMP_FLAGS)
GCC_ALIAS = arm-none-eabi-gcc
CFLAGS = -Wall -Wextra -ffreestanding -mcpu=arm920t -O0 -c
CFLAGS += -g
GCC = $(TOOL_PATH)/$(GCC_ALIAS) $(CFLAGS)

EXCEPT_PATH = /except
EXCEPT_BUILD = $(BUILD_PATH)$(EXCEPT_PATH)
EXCEPT_SOURCE = $(SOURCE_PATH)$(EXCEPT_PATH)

bin/%_s.o: source/%_s.s
	$(GCC) ./$< -o ./$@

bin/%.o: source/%.c
	$(GCC) ./$< -o ./$@

except-hello:
	#
	# EXCEPT
	#

except-clean:
	#
	# EXCEPT CLEAN
	#
	rm -f $(EXCEPT_BUILD)/*.o

except-prep:
	#
	# EXCEPT PREP
	#
	mkdir -p $(EXCEPT_BUILD)
	@echo "Compiling $(EXCEPT_SOURCE) to $(EXCEPT_BUILD) ..."

except-link:
	#
	# EXCEPT LINK
	#
	ls -alh $(EXCEPT_BUILD)
	$(LD) -T$(EXCEPT_SOURCE)/kernel.lds -o $(EXCEPT_BUILD)/kernel $(EXCEPT_BUILD)/*.o $(LIBGCC)
	### Exporting ELF for Debug/Optimization
	### $(OBJDUMP) $(EXCEPT_BUILD)/kernel > $(EXCEPT_BUILD)/kernel.elf
	### $(OBJDUMP) $(EXCEPT_BUILD)/kernel

except: \
	except-hello \
	except-prep \
	$(EXCEPT_BUILD)/ivt_s.o \
	$(EXCEPT_BUILD)/boot.o \
	$(EXCEPT_BUILD)/ex_handlers.o \
	$(EXCEPT_BUILD)/dbgu_drv.o \
	$(EXCEPT_BUILD)/led_drv.o \
	$(EXCEPT_BUILD)/stdlib.o \
	except-link \
	except-clean
	#
	# EXCEPT DONE
	#
	# Run normally:
	# /home/mi/linnert/arm/bin/qemu-bsprak -kernel /home/mi/geroldsch/os_dev/bin/except/kernel
	# Object dump:
	# /home/mi/linnert/arm/bin/arm-none-eabi-objdump -Dhf /home/mi/geroldsch/os_dev/bin/except/kernel
	# Run for GDB:
	# /home/mi/linnert/arm/bin/qemu-bsprak -kernel /home/mi/geroldsch/os_dev/bin/except/kernel -S -gdb tcp::99905
	# GDB
	# /home/mi/linnert/arm/bin/arm-none-eabi-gdb /home/mi/geroldsch/os_dev/bin/except/kernel
	# GDB > target remote : 99905
