/* Betriebssysteme, WS 2017/18, Barry Linnert
 * Author: Gerold Schneider
 */

#include "stdlib.h"
#include "led_drv.h"
#include "dbgu_drv.h"
#include "boot.h"

void main()
{
    led_set_yellow(true);
    led_init();
    dbgu_init();
    led_set_yellow(false);

    int i = 0;
    printf("Welcome to %s!\n", OS_NAME);
    printf("Version %i.%i\n", MAJ_VERSION, MIN_VERSION);
    printf("Ready\n");

    char buffer;

    while(true) {
        buffer = dbgu_in_char();
        if (buffer == 24) {
            break;
        }
        if (buffer != '\0') {
            i++;
            printf("%s #%i: %c %x %b\n", "rx", i, buffer, (unsigned int)buffer, (unsigned int)buffer);
            buffer = '\0';
        }
    }
}