/* Betriebssysteme, WS 2017/18, Barry Linnert
 * Author: Gerold Schneider
 */

#include "errno.h"
#include "stdlib.h"
#include "dbgu_drv.h"
#include <limits.h>
#include <stdarg.h>
#include <float.h>
#include <iso646.h>

// sets a specific bit in an uint32 at a given address
int set_bit(unsigned int addr, unsigned int bit, bool on)
{
    if (bit > 31) {
        error("set_bit invalid argument\n");
        return EINVAL;
    }

    if (on > 0)
        *(volatile unsigned int *) addr |= 1 << bit;
    else
        *(volatile unsigned int *) addr &= ~(1 << bit);

    return 0;
}

int toggle_bit(unsigned int addr, unsigned int bit)
{
    if (bit > 31) {
        error("toggle_bit invalid argument\n");
        return EINVAL;
    }

    *(volatile unsigned int *) addr ^= 1 << bit;

    return 0;
}

int get_bit(unsigned int addr, unsigned int bit)
{
    if (bit > 31) {
        error("get_bit invalid argument\n");
        return EINVAL;
    }

    return (*(volatile unsigned int *) addr >> bit) & 1;
}

void write_u32(unsigned int addr, unsigned int val)
{
    // & reference --> “address of”.
    // * de-reference --> “value pointed by”.

    // set the value of the (volatile unsigned int pointer) addr = val
    *(volatile unsigned int *) addr = val;
}

unsigned int read_u32(unsigned int addr)
{
    return *(volatile unsigned int *) addr;
}

// returns a pointer to the first occurence of needle in haystack, else a null pointer
char *strchr(char *haystack, const char needle)
{
    if (haystack == NULL) {
        error("strchr invalid argument\n");
        return NULL;
    }

    char *cursor = haystack;
    for (; *cursor; cursor++)
        if (*cursor == needle)
            return cursor;
    return NULL;
}

// returns the length of a given string
int strlen(const char *str)
{
    if (str == NULL) {
        error("strlen invalid argument\n");
        return 0;
    }

    int i = 0;
    for (; *str; str++) {
        i++;
    }
    return i;
}

char *strrev(char *str)
{
    if (str == NULL) {
        error("strrev invalid argument\n");
        return NULL;
    }

    char *p1, *p2;

    if (!str || !*str)
        return str;
    for (p1 = str, p2 = str + strlen(str) - 1; p2 > p1; ++p1, --p2) {
        *p1 ^= *p2;
        *p2 ^= *p1;
        *p1 ^= *p2;
    }
    return str;
}

// converts an integer value into an ascii representation in any base between 1 and 16;
char *itoa(const unsigned int radix, const int num, char *buffer)
{
    if (radix > 16) {
        error("itoa invalid argument\n");
        return NULL;
    }

    if (buffer == NULL) {
        error("itoa invalid argument\n");
        return NULL;
    }

    const char hexmap[18] = "0123456789ABCDEF";
    int rem = num;

    // add '-' if value is negative and make rem absolute
    if (num < 0) {
        rem = -num;
        // Note: the sign is included in the return value of idigit, so no need to push len.
    }

    char *s = &buffer[0];    // make a cursor, put it at the end (points to '\0')

    // repeatedly divide value by base, adding the remainder of each iteration to out_str
    do {
        *s = hexmap[rem % radix];
        s++;
        rem /= radix;
    }
    while (rem > 0);

    if (num < 0) {
        *s = '-';
        s++;
    }


    // terminate the buffer
    *s = '\0';

    strrev(buffer);

    return buffer;
}

float pow(float base, int exp)
{
    float res = 1;
    for (; exp; exp--) {
        res *= base;
    }
    return res;
}

// converts a float to its ascii representation
char *ftoa(const double num, char buffer[], const int digits)
{
    if (digits > 9) {
        error("ftoa invalid argument\n");
        return NULL;
    }

    int int_part = (int) num;
    double float_part = num - (double) int_part;
    float_part = float_part * pow(10, digits);

    if (float_part < 0) {
        float_part = -float_part;
        buffer[0] = '-';
        itoa(10, int_part, &buffer[1]);
    }
    else {
        itoa(10, int_part, buffer);
    }

    int point_idx = strlen(buffer);
    buffer[point_idx] = '.';

    itoa(10, (int) float_part, &buffer[point_idx + 1]);

    return buffer;
}

// copies a string from source to dest and returns a pointer to dest[].
char *strcpy(const char source[], char dest[])
{
    int i = 0;
    while ((dest[i] = source[i]) != '\0') {
        i++;
    }
    return &dest[0];
}

// copies a string from source to dest and returns a pointer to the end of the copied source inside dest[].
char *strcpyr(const char source[], char dest[])
{
    int i = 0;
    while ((dest[i] = source[i]) != '\0')
        i++;

    return &dest[i];
}

// prints a formatted string to dbgu_out;
__attribute__((format(printf, 1, 2)))
void printf(const char *format, ...)
{
    /*
     * 1: Position des Formatstrings in der Argumentliste
     * 2: Position des ersten gegen den Formatstring zu prüfenden Arguments
     */
    va_list args;
    va_start(args, format);

    char out_str[512];
    char *s = &out_str[0]; // cursor pointing to the first char in out_str
    char numstr[33];

    for (; *format; format++) {
        if (*format == '%') {
            format++;
            strcpy("________________________________", numstr); // dots for debugging
            switch (*format) {
                case 'u':;// Unsigned decimal integer
                    unsigned int arg_u = va_arg(args, unsigned int);
                    itoa(10, arg_u, numstr);
                    s = strcpyr(numstr, s);
                    break;
                case 'd':; // Signed decimal integer
                case 'i':; // Signed decimal integer
                    int arg_i = va_arg(args, int);
                    itoa(10, arg_i, numstr);
                    s = strcpyr(numstr, s);
                    break;
                case 'x':; // Unsigned hexadecimal integer
                    int arg_x = va_arg(args, unsigned int);
                    itoa(16, arg_x, numstr);
                    s = strcpyr("0x", s);
                    s = strcpyr(numstr, s);
                    break;
                case 'b':; // binary
                    int arg_b = va_arg(args, unsigned int);
                    itoa(2, arg_b, numstr);
                    s = strcpyr("0b", s);
                    s = strcpyr(numstr, s);
                    break;
                case 'f':; // Decimal floating point
                    double arg_f = va_arg(args, double);
                    ftoa(arg_f, numstr, 9);
                    s = strcpyr(numstr, s);
                    break;
                case 'c':; // Character
                    *s = va_arg(args, int);
                    s++;
                    *s = '\0';
                    break;
                case 's':; // String of characters
                    char *arg_s = va_arg(args, char*);
                    s = strcpyr(arg_s, s);
                    break;
                default:
                    dbgu_out_str("PRINTF_ERROR");
                    break;
            }
        }
        else {
            *s = *format;
            s++;
            *s = '\0';
        }
    }
    dbgu_out_str(out_str);
    va_end(args);
}

void error(char *message)
{
    if (message == NULL) {
        printf("ERROR");
        return;
    }
    printf("ERROR: %s", message);
}