/* Betriebssysteme, WS 2017/18, Barry Linnert
 * Author: Gerold Schneider
 */

#ifndef LED_DRV_H
#define LED_DRV_H

// PIO Registers
#define PIOA 0xfffff400 // PIO A
#define PIOB 0xfffff600 // PIO B
#define PIOC 0xfffff800 // PIO C

// Generic PIO Memory Map
struct pio_mmap {
    unsigned int per;
    unsigned int unused0[3];
    unsigned int oer;
    unsigned int unused1[7];
    unsigned int sodr;
    unsigned int codr;
};

// PIO Register Offsets
//#define PIO_PER 0x00
//#define PIO_OER 0x10
//#define PIO_SODR 0x30

// LED Bits
#define LED_YELLOW (1 << 27)
#define LED_RED    (1 << 0)
#define LED_GREEN  (1 << 1)

void led_init();
void led_set_red(bool on);
void led_set_green(bool on);
void led_set_yellow(bool on);

#endif
