/* Betriebssysteme, WS 2017/18, Barry Linnert
 * Author: Gerold Schneider
 */

#ifndef BOOT_H
#define BOOT_H
#include "stdlib.h"

#define MAJ_VERSION 0
#define MIN_VERSION 1
#define OS_NAME "OMIN_OS"

void main();

#endif //OSONE_BOOT_H
