/* Betriebssysteme, WS 2017/18, Barry Linnert
 * Author: Gerold Schneider
 */

#ifndef DBGU_DRV_H
#define DBGU_DRV_H

// DBGU Memory Map
struct dbgu_mmap {
    unsigned int cr;
    unsigned int mr;
    unsigned int ier;
    unsigned int idr;
    unsigned int imr;
    unsigned int sr;
    char rhr;
    char unused0[3];
    char thr;
    char unused1[3];
    unsigned short brgr;
};

// DBGU Register Offsets
// #define DBGU_CR 0x0000  // Debug Unit Control Register
// #define DBGU_MR 0x0004  // Debug Unit Mode Register
// #define DBGU_SR 0x0014  // Debug Unit Status Register
// #define DBGU_RHR 0x0018 // Debug Unit - Receive Holding Register
// #define DBGU_THR 0x001C // Debug Unit - Transmit Holding Register

// DBGU Control Register Bits
#define DBGU_CR_RSTSTA 1 << 8 // DBGU_CR Transmitter Disable
#define DBGU_CR_TXDIS 1 << 7 // DBGU_CR Transmitter Disable
#define DBGU_CR_TXEN  1 << 6 // DBGU_CR Transmitter Enable
#define DBGU_CR_RXDIS 1 << 5 // DBGU_CR Receiver Disable
#define DBGU_CR_RXEN  1 << 4 // DBGU_CR Receiver Enable
#define DBGU_CR_RSTTX 1 << 3 // DBGU_CR Reset Transmitter
#define DBGU_CR_RSTRX 1 << 2 // DBGU_CR Reset Receiver
// DBGU Status Register Bits
#define DBGU_SR_RXRDY 1 << 0 // DBGU_SR Receiver Ready
#define DBGU_SR_TXRDY 1 << 1 // DBGU_SR Transmitter Ready
#define DBGU_SR_ENDRX 1 << 3 // DBGU_SR End of Receiver Transfer
#define DBGU_SR_ENDTX 1 << 4 // DBGU_SR End of Transmitter Transfer
#define DBGU_SR_OVRE  1 << 5 // DBGU_SR Overrun Error
#define DBGU_SR_FRAME 1 << 6 // DBGU_SR Framing Error
#define DBGU_SR_PARE  1 << 7 // DBGU_SR Parity Error
#define DBGU_SR_TXEMPTY 1 << 9 // DBGU_SR Transmitter Empty
#define DBGU_SR_TXBUFE 1 << 11 // DBGU_SR Transmission Buffer Empty
#define DBGU_SR_RXBUFF 1 << 12 // DBGU_SR Receive Buffer Full
#define DBGU_SR_COMMTX 1 << 30 // DBGU_SR Debug Communication Channel Write Status
#define DBGU_SR_COMMRX 1 << 31 // DBGU_SR Debug Communication Channel Read Status

// Memory Mapped IO Offset
#define DBGU_ADDR 0xFFFFF200 // Debug Unit

void dbgu_init();
bool dbgu_out_str(const char data[]);
bool dbgu_out_char(const char data);
char dbgu_in_char();
void dbgu_reset_status();

#endif
