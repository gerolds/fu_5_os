/* Betriebssysteme, WS 2017/18, Barry Linnert
 * Author: Gerold Schneider
 */

#include "errno.h"
#include "stdlib.h"
#include "dbgu_drv.h"

static volatile struct dbgu_mmap *const _dbgu = (struct dbgu_mmap *) DBGU_ADDR;

void dbgu_init()
{
    // Init dbgu
    _dbgu->cr = DBGU_CR_RSTRX | DBGU_CR_RSTTX; // reset rx and tx
    _dbgu->cr = DBGU_CR_RSTSTA; // reset status bits
    _dbgu->cr = DBGU_CR_RXEN; // enable receiver
    _dbgu->cr = DBGU_CR_TXEN; // enable transmitter
}

bool dbgu_out_char(const char data)
{
    if (_dbgu->sr & DBGU_SR_TXRDY && (int) data > 0) {
        _dbgu->thr = data;
        return true;
    }
    else {
        return false;
    }
}

bool dbgu_out_str(const char data[])
{
    int i;
    for (i = 0; data[i] != '\0'; i++) {
        if (data[i] == '\0')
            break;
        dbgu_out_char(data[i]);
        if (i > 512)
            return false;
    }
    return true;
}

char dbgu_in_char()
{
    if (_dbgu->sr & DBGU_SR_RXRDY) {
        return _dbgu->rhr;
    }
    return (char) 0;
}

void dbgu_reset_status()
{
    _dbgu->cr |= DBGU_CR_RSTSTA;
    _dbgu->cr &= ~DBGU_CR_RSTSTA;
}