/* Betriebssysteme, WS 2017/18, Barry Linnert
 * Author: Gerold Schneider
 */

#ifndef STDLIB_H
#define STDLIB_H

#define NULL ( (void *) 0)

// make us some booleans
typedef enum { false, true } bool;

// sets a specific bit in an uint32 at a given address
int set_bit(unsigned int addr, unsigned int bit, bool on);
int toggle_bit(unsigned int addr, unsigned int bit);
int get_bit(unsigned int addr, unsigned int bit);
void write_u32(unsigned int addr, unsigned int val);
unsigned int read_u32(unsigned int addr);

void printf(const char *format, ...);
char* strcpyr(const char source[], char dest[]);
char* strcpy(const char source[], char dest[]);
char* strchr(char* haystack, const char needle);
int strlen(const char *str);

char *btoa(const unsigned int radix, const int num, char* buffer);
char *ftoa(const double num, char buffer[], const int digits);

float pow(float base, int exp);
void error(char *message);

#endif