.set  MODE_USR, 0x10                    /* Normal User Mode                                        */
.set  MODE_FIQ, 0x11                    /* FIQ Processing Fast Interrupts Mode                     */
.set  MODE_IRQ, 0x12                    /* IRQ Processing Standard Interrupts Mode                 */
.set  MODE_SVC, 0x13                    /* Supervisor Processing Software Interrupts Mode          */
.set  MODE_ABT, 0x17                    /* Abort Processing memory Faults Mode                     */
.set  MODE_UND, 0x1B                    /* Undefined Processing Undefined Instructions Mode        */
.set  MODE_SYS, 0x1F                    /* System Running Priviledged Operating System Tasks Mode  */

.set  I_BIT, 0x80                       /* when I bit is set, IRQ is disabled (program status registers) */
.set  F_BIT, 0x40                       /* when F bit is set, FIQ is disabled (program status registers) */

.section .init
    _ivt:
        LDR pc, Reset_addr       // 0x00000000 branch to reset -> main
        LDR pc, Undef_addr       // 0x00000004 offset pc to _ex_undefined
        LDR pc, SWI_addr         // 0x00000008 offset pc to _ex_swi
        LDR pc, Prefetch_addr    // 0x0000000C offset pc to _ex_prefetch
        LDR pc, Data_addr        // 0x00000010 offset pc to _ex_data
        NOP   	                 // 0x00000014 (unused)
        LDR pc, IRQ_addr         // 0x00000018 offset pc to _ex_irq
        LDR pc, FIQ_addr         // 0x0000001C offset pc to _ex_fiq
                                 // Note:
                                 // 'LDR pc, _label' gets translated to 'LDR pc, [pc #]' where
                                 // # is the _label's offset relative to pc.

    Reset_addr:     .word _start
    Undef_addr:     .word ex_handler_undefined
    SWI_addr:       .word ex_handler_swi
    Prefetch_addr:  .word ex_handler_prefetch
    Data_addr:      .word ex_handler_data
    IRQ_addr:       .word ex_handler_irq
    FIQ_addr:       .word ex_handler_fiq

    .global _start
    _start:
        // Initializing Stack Pointers
                                    // Enter each processor mode and set up the stack pointer
                                    // Ref.: ARM-ARM.pdf, A2.5, p49ff
        MSR     CPSR_c, #MODE_UND|I_BIT|F_BIT
        LDR     sp, __stack_und_base
        MSR     CPSR_c, #MODE_ABT|I_BIT|F_BIT
        LDR     sp, __stack_abt_base
        MSR     CPSR_c, #MODE_FIQ|I_BIT|F_BIT
        LDR     sp, __stack_fiq_base
        MSR     CPSR_c, #MODE_IRQ|I_BIT|F_BIT
        LDR     sp, __stack_irq_base
        MSR     CPSR_c, #MODE_SYS|I_BIT|F_BIT
        LDR     sp, __stack_sys_base
        MSR     CPSR_c, #MODE_SVC|I_BIT|F_BIT
        LDR     sp, __stack_svc_base
                                    //   ... back in svc mode
        LDR r0, =main
        BX r0

    __stack_und_base:  .word 0x23FFFFFC
    __stack_abt_base:  .word 0x23FFEFFC // - 0xFFF
    __stack_fiq_base:  .word 0x23FFDFFC // - 0xFFF
    __stack_irq_base:  .word 0x23FFCFFC // - 0xFFF
    __stack_svc_base:  .word 0x23FFBFFC // - 0xFFF
    __stack_sys_base:  .word 0x23FFAFFC // - 0xFFF

    .Lend:
        b .Lend
