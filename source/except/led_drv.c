/* Betriebssysteme, WS 2017/18, Barry Linnert
 * Author: Gerold Schneider
 */

#include "stdlib.h"
#include "led_drv.h"

static volatile struct pio_mmap * const _piob = (struct pio_mmap *) PIOB;
static volatile struct pio_mmap * const _pioc = (struct pio_mmap *) PIOC;

void led_init()
{
    _piob->per = LED_YELLOW;
    _piob->oer = LED_YELLOW;
    _pioc->per = LED_RED;
    _pioc->oer = LED_RED;
    _pioc->per = LED_GREEN;
    _pioc->oer = LED_GREEN;
}

void led_set_red(bool on)
{
    if (on)
        _pioc->sodr = LED_RED;
    else
        _pioc->codr = LED_RED;
}

void led_set_green(bool on)
{
    if (on)
        _pioc->sodr = LED_GREEN;
    else
        _pioc->codr = LED_GREEN;
}

void led_set_yellow(bool on)
{
    if (on)
        _pioc->sodr = LED_YELLOW;
    else
        _pioc->codr = LED_YELLOW;
}