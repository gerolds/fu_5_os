/* Betriebssysteme, WS 2017/18, Barry Linnert
 * Author: Gerold Schneider
 */

// Errorcodes based on standard linux errorcodes

#ifndef ERRNO_H
#define ERRNO_H

#define EPERM        1  /* Operation not permitted */
#define ESRCH        3  /* No such process */
#define EINTR        4  /* Interrupted system call */
#define EIO          5  /* I/O error */
#define ENXIO        6  /* No such device or address */
#define E2BIG        7  /* Argument list too long */
#define ENOEXEC      8  /* Exec format error */
#define EAGAIN      11  /* Try again */
#define ENOMEM      12  /* Out of memory */
#define EACCES      13  /* Permission denied */
#define EFAULT      14  /* Bad address */
#define ENOTBLK     15  /* Block device required */
#define EBUSY       16  /* Device or resource busy */
#define ENODEV      19  /* No such device */
#define EINVAL      22  /* Invalid argument */
#define ENOSPC      28  /* No space left on device */
#define EDOM        33  /* Math argument out of domain of func */
#define ERANGE      34  /* Math result not representable */
#define EDEADLK     35  /* Resource deadlock would occur */
#define ENOSYS      38  /* Function not implemented */
#define EWOULDBLOCK EAGAIN  /* Operation would block */
#define EIDRM       43  /* Identifier removed */
#define EBADRQC     56  /* Invalid request code */
#define EBADSLT     57  /* Invalid slot */
#define EDEADLOCK   EDEADLK
#define ENOSTR      60  /* Device not a stream */
#define ENODATA     61  /* No data available */
#define ETIME       62  /* Timer expired */
#define ENOSR       63  /* Out of streams resources */
#define ECOMM       70  /* Communication error on send */
#define EPROTO      71  /* Protocol error */
#define EBADMSG     74  /* Not a data message */
#define EOVERFLOW   75  /* Value too large for defined data type */
#define EMSGSIZE    90  /* Message too long */
#define ECANCELED   125 /* Operation Canceled */
#define ENOTRECOVERABLE 131 /* State not recoverable */

#endif