//
// Created by Gerold Schneider on 19.11.17.
//

#include "ex_handlers.h"
#include "dbgu_drv.h"

// r13 = sp
// r14 = lr
// r15 = pc

//ARM-ARM.pdf A2.6
void ex_handler_reset(void)
{
    printf("ir_reset");
}

void ex_handler_undefined(void)
{
    //http://infocenter.arm.com/help/index.jsp?topic=/com.arm.doc.ddi0363g/Beijdcef.html
    //http://infocenter.arm.com/help/index.jsp?topic=/com.arm.doc.ddi0290g/ch02s08s14.html
    //A3-16
    register unsigned int *lnk_ptr;
    asm volatile("sub %0, lr, #4" : "=r"(lnk_ptr));     // get aborting instruction
    //printf("Exception: Undefined Instruction %x @ %p\n", *lnk_ptr, lnk_ptr);
    printf("Exception: Undefined Instruction %x\n", *lnk_ptr);

    //printf("Exception: Undefined Instruction\n");
}

void ex_handler_swi(void)
{
    printf("Exception: Software Interrupt\n");
}

void ex_handler_prefetch(void)
{
    printf("Exception: Prefetch Abort\n");
    asm volatile(
                    "SUBS pc, lr, #4\n"
    );
}

void ex_handler_data(void)
{
    register unsigned int *lnk_ptr;
    asm volatile(
                    "SUB    %0, lr, #4\n"     : "=r"(lnk_ptr)    // get aborting instruction
    );
    printf("Exception: Data Abort %x @ %p |\n", *lnk_ptr, lnk_ptr);
}

void ex_handler_irq(void)
{
    //http://infocenter.arm.com/help/index.jsp?topic=/com.arm.doc.ddi0363g/BEIDDFBB.html
    printf("Exception: Interrupt\n");
}

void ex_handler_fiq(void)
{
    dbgu_out_char('F');
    printf("Exception: Fast Interrupt\n");
}