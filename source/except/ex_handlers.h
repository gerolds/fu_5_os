//
// Created by Gerold Schneider on 19.11.17.
//

#ifndef OS_DEV_EX_HANDLERS_H
#define OS_DEV_EX_HANDLERS_H

#include "stdlib.h"

void ex_handler_reset(void);
void ex_handler_undefined(void);
void ex_handler_swi(void);
void ex_handler_prefetch(void);
void ex_handler_data(void);
void ex_handler_irq(void);
void ex_handler_fiq(void);

#endif //OS_DEV_EX_HANDLERS_H
