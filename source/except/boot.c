/* Betriebssysteme, WS 2017/18, Barry Linnert
 * Author: Gerold Schneider
 */

#include "stdlib.h"
#include "led_drv.h"
#include "dbgu_drv.h"
#include "boot.h"

static unsigned int MC_ADDRESS = 0xffffff00;
static unsigned int MC_RCR = 0x00;
//page 129
static unsigned int MC_RCR_RCB = 1 << 0;

void remap_memory(void) {
    //writing the MC_RCR(Remap Control Register) RCB field to one
    int *p = (int *) (MC_ADDRESS + MC_RCR);
    *p = MC_RCR_RCB;
}

void remap() {
    volatile unsigned int *p = (volatile unsigned int *) 0xffffff00;
    *p = 1;
}

void check_ivt() {
    printf("-- IVT --------------------\n");
    printf("Reset     %p %x\n", (volatile unsigned int *) 0x00000000, *((volatile unsigned int *) 0x00000000));
    printf("Undef     %p %x\n", (volatile unsigned int *) 0x00000004, *((volatile unsigned int *) 0x00000004));
    printf("SWI       %p %x\n", (volatile unsigned int *) 0x00000008, *((volatile unsigned int *) 0x00000008));
    printf("Prefetch  %p %x\n", (volatile unsigned int *) 0x0000000C, *((volatile unsigned int *) 0x0000000C));
    printf("Data      %p %x\n", (volatile unsigned int *) 0x00000010, *((volatile unsigned int *) 0x00000010));
    printf("IRQ       %p %x\n", (volatile unsigned int *) 0x00000018, *((volatile unsigned int *) 0x00000018));
    printf("FIQ       %p %x\n", (volatile unsigned int *) 0x0000001C, *((volatile unsigned int *) 0x0000001C));
    printf("---------------------------\n");
}

void main()
{
    remap_memory();
    check_ivt();

    led_set_yellow(true);
    led_init();
    dbgu_init();
    led_set_yellow(false);

    int i = 0;
    printf("Welcome to %s", OS_NAME);
    printf(" %i.%i\n", MAJ_VERSION, MIN_VERSION);
    printf("\nNew features in this release:\n");
    printf("    Press 'd' for Data Abort\n");
    printf("    Press 's' for SWI\n");
    printf("    Press 'u' for Undefined Instruction\n");
    printf("    Press 'p' for Prefetch Abort\n");
    // printf("    Press 'i' to enable DBGU_ENDRX interrupts\n");
    // printf("    Press 'o' to disable DBGU_ENDRX interrupts\n");
    printf("Happy exceptions!\n\n");
    printf("Ready\n");

    char buffer;

    while(true) {
        buffer = dbgu_in_char();
        if (buffer == 24) {
            break;
        }
        if (buffer != '\0') {
            i++;
            printf("%s #%i: %c %x\n", "rx", i, buffer, (unsigned int)buffer);
        } else {
            // what now?
        }

        switch (buffer) {
            case 's':;
                // swi
                printf("Forcing Software Interrupt ...\n");
                asm volatile("swi 01\n");
                printf("... and we're back!\n");
                break;
            case 'u':;
                // undefined instruction
                printf("Forcing Undefined Instruction ...\n");

                asm volatile(".word 0x7f000f0\n"); // permanently undefined ARM-ARM.pdf, p148

                printf("'.word 0x7f000f0' was executed.\n");
                break;
            case 'd':;
                // data abort
                printf("Forcing Data Abort ...\n");

                // make a pointer to an undefined memory address:
                // 0x00400000 - 0x0FFFFFFF are undefined.
                // 0x90000000 - 0xEFFFFFFF are undefined.
                volatile unsigned int* undef = (volatile unsigned int*)0x90000000;
                *undef = 1;

                printf("... and back to known memory.\n");
                break;
/*
            case 'i':;
                // interrupt
                dbgu_enable_rx_interrupt();
                break;
            case 'o':;
                // interrupt
                dbgu_disable_rx_interrupt();
                break;
*/
            case 'p':;
                // prefetch abort
                printf("Prefetch abort not implemented yet.\n");
                // how do we cause this?
                break;
            default:
                // nothing
                break;
        }
    }
}