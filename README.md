# README #

Work in progres.

## Basic Workflow ##

### Idea ###

We develop locally with __CLion__ (or any other IDE or text editor), use __git__ for version control and
collaboration, a __Bitbucket__ repository and deployment via __rsync__ to the remote test server
```<host>.imp.fu-berlin.de```. The default for ```<host>``` is ```andorra```.

We'll assume the project lives in ```~/os_dev``` on both, the local pc and and ```<host>.imp.fu-berlin.de```.
This means each project member deploys to his own ```~/``` directory on ```<host>.imp.fu-berlin.de```. This enables us to use the same Makefiles and shell-scripts while still keeping everyting separate during individual development cycles.

All paths in shell scripts should assume the script is run from the project root (```~/os_dev``` by default).

Project directory structure:

```
./                # current Makefile and global shell-scripts
./source          # all source code
./source/<module> # module specific source code and Makefile (aka. old Makefiles)
./bin             # builds
./bin/<module>    # module specific builds
./doc             # documentation
```

The __Bitbucket__ repo also includes a __Trello__ board. If neccessary we use that for ad-hoc Kanban (todo-doing-done)
style work management.

**Setup Notes**

On ```local```: Setup the project (only once)

```
git clone https://bitbucket.org/gerolds/fu_5_os ~/
mv ~/fu_5_os ~/os_dev
```

On ```local```: Log into ```<host>.imp.fu-berlin.de```

```
ssh <username>@<host>.imp.fu-berlin.de
```

On ```<host>.imp.fu-berlin.de```: Create a symlink to the toolchain in ```/home/mi/linnert/arm``` and setup project
directory, just for convenience.

```
ln -s /home/mi/linnert/arm ~/arm
mkdir ~/os_dev
```

**Edit, Build, Test and Run**

On ```local```: Manually __rsync__ files to ```<host>.imp.fu-berlin.de```
```
rsync -rpthvkL --del --delete --exclude=.* ~/os_dev/* <username>@<host>.imp.fu-berlin.de:~/os_dev
```

On ```local```: __Deploy__ via shell-script ```deploy```
```
~/os_dev/deploy
```

On ```<host>.imp.fu-berlin.de```: __gcc__
```
~/arm/bin/arm-none-eabi-gcc -Wall -Wextra -ffreestanding -mcpu=arm920t -O2 -c ~/os_dev/source/foo/bar.c
```

On ```<host>.imp.fu-berlin.de```: __Build__
```
cd ~/os_dev
make foo
```

On ```<host>.imp.fu-berlin.de```: __objdump__ ```bar.o``` to ```bar.elf```
```
~/arm/bin/arm-none-eabi-objdump -d ~/os_dev/bin/foo/bar.o > ~/os_dev/bin/foo/bar.elf
```

On ```<host>.imp.fu-berlin.de```: __Run__
```
~/arm/bin/qemu-bsprak -kernel ~/os_dev/bin/foo/bar
```

### Coding Style ###

We'll use the K&R Coding style (Preset in CLion, suject to change). Sample:

```c
#include <stdio.h>
int main()
{
    int n, i, flag = 0;

    printf("Enter a positive integer: ");
    scanf("%d", &n);

    for (i = 2; i <= n / 2; ++i) {
        // condition for nonprime number
        if (n % i == 0) {
            flag = 1;
            break;
        }
    }

    if (flag == 0)
        printf("%d is a prime number.", n);
    else
        printf("%d is not a prime number.", n);

    return 0;
}
```

#### Naming conventions:

* ```UNDERSCORED_UPPER_CASE``` for macro definitions and constants
* ```underscored_lower_case``` for variables and functions
* __Namespaces:__ C has no concept of namespaces, to still keep modules nicely separated, a function ```bar()``` inside a
file ```foo_something.c``` should be called: ```foo_bar()```. Namespaces should have very short names.
* __Word Order:__ More specific words first. Like this: ```set_green_led()```, NOT like this: ```led_green_set()```.
Exception: Namespaces should always be a prefix to the descriptive function name. Makes it easier to read and understand
what the code is doing.
* __Private:__ Prefix any variable, macro, constant or function with an underscore (```_foo```) if it should not be called
from outside the source file or namespace in which it is defined.
